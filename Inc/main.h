/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2019 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */
typedef struct MotorSpeedCalcParams
{
    uint32_t Ch1Rising;
    uint32_t Ch1Falling;
    
    uint32_t Freq;
    uint32_t DutyCycle;
    
    uint32_t Speed;
    
}SpeedCalcParams;

typedef enum {
	E_SUCCESS = 0,
	E_FAIL,
	E_BUSY,
	E_UNKNOWN,
	E_OUT_OF_RANGE,
	E_DRIVER_EMPTY,
	E_DRIVER_VALUE_INVALID,
}Error_t;

typedef enum
{
    eMotor1 = 0,
    eMotor2 = 1,
    eMotorMax
}enmMotorDriver;

typedef enum
{
    eMotorForward = 0,
    eMotorBackward = 1,
    eMotorMovMax
}enmMotorMovDiredtion;
typedef struct
{
	uint32_t DutyCycle;
	uint32_t SpeedRef;
	int32_t PreviousError;
	int32_t ErrorSum;
    int32_t PropPart;
    int32_t IntegralPart;
    int32_t AntiWindUpCoef;
	float   Kp;
	float   Kd;
	float   Ki;
}SpeedPIDParams;

typedef struct
{
    float Kp;
    float Ki;
    float Kfeedback;
    float IntegralPart;
    float PropPart;
    float AntiWindUpCoef;
    float AntiWindUpLimitSpeed;
}PIParameters;

struct  sMotorParams{      // bits   description

    uint16_t    motorDirection:1;
    uint16_t    motorSpeed:15;
};

/* RX Union for bit field */
typedef union {
  struct sMotorParams motorParams;
  uint16_t u16allData;
  uint8_t u08allData[2];
}motor1_;

/* RX Union for bit field */
typedef union {
  struct sMotorParams motorParams;
  uint16_t u16allData;
  uint8_t u08allData[2];
}motor2_;

/* Uart RX  Structure */
typedef struct {      // bits   description
   uint8_t startByte;
   motor1_ motor1;
   motor2_ motor2;
   uint8_t endByte;
  
}sUartRX;

/* Uart TX  Structure */
typedef struct {      // bits   description
   uint8_t startByte;
   motor1_ motor1;
   motor2_ motor2;
   uint8_t endByte;
  
}sUartTX;

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */
#if !defined(LIMIT_VARIABLE)
#define sLIMIT_VARIABLE(x, max, min) { if(x > max) {x = max;}else if(x < min)	{x = min;}}
#endif
/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define Motor2PWM1_Pin GPIO_PIN_6
#define Motor2PWM1_GPIO_Port GPIOA
#define Motor2PWM2_Pin GPIO_PIN_7
#define Motor2PWM2_GPIO_Port GPIOA
#define MotorPWM1_Pin GPIO_PIN_10
#define MotorPWM1_GPIO_Port GPIOB
#define MotorPWM2_Pin GPIO_PIN_11
#define MotorPWM2_GPIO_Port GPIOB
#define Motor1INPC_Pin GPIO_PIN_8
#define Motor1INPC_GPIO_Port GPIOA
#define Motor2INPC_Pin GPIO_PIN_6
#define Motor2INPC_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

#define NUMBER_OF_MOTORS    2
#define SAMPLE_DATA_COUNT   128 //32 //128 //20 //10

#define APP_SENSOR_CONTROL_MAX_STD_DEVIATION 	100

#define APP_PI_KP (float)0.002 //0.003 //Debug 18.06.11 UO 0.01 //Debug 18.02.12 ES 0.03 //0.1           //0.01
#define APP_PI_KI (float)0.00001 //Debug 18.02.12 ES 0.0005        //0.00015
#define APP_PI_KD (float)0.0

#define PI				3.141592654f
#define TWOPI			((float)(PI*2.0f))
#define SP2W            ((float)(TWOPI/15.0f)) 
    
#define RECEIVE_START_BYTE 0xFF

//#define OPEN_TEST
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
