float Saturate(float x, float llim, float hlim){
	if(x < llim)return llim;
	else if (x > hlim)return hlim;
	else return x;
}