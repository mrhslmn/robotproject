/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2019 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <string.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim4;

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;

osThreadId Motor1ControlHandle;
osThreadId Motor2ControlHandle;
osThreadId INPC1Handle;
osThreadId INPC2Handle;
osThreadId UartRXHandle;
osThreadId UartTXHandle;
osMessageQId motorSpeedHandle;
osTimerId Timer1msHandle;
osTimerId Timer100msHandle;
osMutexId xTxMutexHandle;
osStaticMutexDef_t xTxMutexControlBlock;
osSemaphoreId xRxSemaphoreHandle;
osStaticSemaphoreDef_t xRxSemaphoreControlBlock;
/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

//Timers
TimerHandle_t xTimers[2];

static SpeedCalcParams CalcParams_t[NUMBER_OF_MOTORS];
static SpeedPIDParams PIDParams_t[NUMBER_OF_MOTORS];
static uint32_t MotorSpeedValuesArray[eMotorMax][SAMPLE_DATA_COUNT];

//Global Variables
int16_t i16PIDErrorLimit = 200;
int32_t i32PIDErrorSumLimit = 10000;
uint16_t i16PIDMinOnTime = 0;
uint16_t i16PIDMaxOnTime = 800; //950;

uint16_t u16MotorSpeedMaxValue = 6500;
uint16_t u16MotorSpeedMinValue = 0;

int32_t adc_filter_counter[eMotorMax];

float AppPIKp = APP_PI_KP;
float AppPIKi = APP_PI_KI;
float AppPIKd = APP_PI_KD;

uint32_t u32OpenDutyCycle = 0;

uint32_t SpeedValues[eMotorMax];
uint32_t SpeedMeanValues[eMotorMax];
uint32_t MotorEncPulseCnt[eMotorMax];

uint8_t u8MotorDirection[eMotorMax];

uint8_t u8SemaphoreCnt;

sUartRX rx;

sUartTX tx;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_TIM1_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM3_Init(void);
static void MX_TIM4_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_USART1_UART_Init(void);
void vTaskMotor1Control(void const * argument);
void vTaskMotor2Control(void const * argument);
void vTaskINPC1(void const * argument);
void vTaskINPC2(void const * argument);
void vTaskUartRX(void const * argument);
void vTaskUartTX(void const * argument);
void vTimer1Callback1(void const * argument);
void vTimer100msCallBack(void const * argument);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void app_get_statistics(uint32_t *data, uint32_t size, uint32_t *outMedian, uint32_t *outMean, uint32_t *outDeviation);
void ResetPID(enmMotorDriver driver );
void CalculateSpeedMean(enmMotorDriver driver);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
uint8_t rx_data[6]; /* uart5 data */
static uint8_t data_array[6];
static uint8_t index = 0;

uint8_t tx_data[6]; /* uart5 data */
static uint8_t tx_data_array[6];


void HAL_UART_RxCpltCallback (UART_HandleTypeDef * huart)
{
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    if(huart->Instance == USART2)

    //data_array[index++] = rx_data[0];
    memcpy(data_array, rx_data, 6);
    xSemaphoreGiveFromISR(xRxSemaphoreHandle, &xHigherPriorityTaskWoken);
    //index++;

    HAL_UART_Receive_IT(&huart2, rx_data, 6);

 }
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  MX_TIM4_Init();
  MX_USART2_UART_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */
    HAL_TIM_IC_Start(&htim1, TIM_CHANNEL_1);
    HAL_TIM_IC_Start(&htim1, TIM_CHANNEL_2);
    HAL_TIM_Base_Start(&htim1);
    
    HAL_TIM_IC_Start(&htim4, TIM_CHANNEL_1);
    HAL_TIM_IC_Start(&htim4, TIM_CHANNEL_2);
    HAL_TIM_Base_Start(&htim4);
    
    HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_3);
    HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_4);
    
    HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);
    HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2);
    
//    HAL_UART_Transmit(&huart2,"Naber!",6,100);
//    HAL_UART_Transmit(&huart2,"\n",1,100);

    HAL_UART_Receive_IT(&huart2, rx_data, 6);

    PIDParams_t[eMotor1].SpeedRef = 0;
    PIDParams_t[eMotor2].SpeedRef = 0;
    
    ResetPID(eMotor1);
    ResetPID(eMotor2);
       
    u8MotorDirection[eMotor1] = 0;
    u8MotorDirection[eMotor2] = 0;
    
    
    
  /* USER CODE END 2 */

  /* Create the mutex(es) */
  /* definition and creation of xTxMutex */
  osMutexStaticDef(xTxMutex, &xTxMutexControlBlock);
  xTxMutexHandle = osMutexCreate(osMutex(xTxMutex));

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* Create the semaphores(s) */
  /* definition and creation of xRxSemaphore */
  osSemaphoreStaticDef(xRxSemaphore, &xRxSemaphoreControlBlock);
  xRxSemaphoreHandle = osSemaphoreCreate(osSemaphore(xRxSemaphore), 1);

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* Create the timer(s) */
  /* definition and creation of Timer1ms */
  osTimerDef(Timer1ms, vTimer1Callback1);
  Timer1msHandle = osTimerCreate(osTimer(Timer1ms), osTimerPeriodic, NULL);

  /* definition and creation of Timer100ms */
  osTimerDef(Timer100ms, vTimer100msCallBack);
  Timer100msHandle = osTimerCreate(osTimer(Timer100ms), osTimerPeriodic, NULL);

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
    osTimerStart(Timer1msHandle,  1);
    osTimerStart(Timer100msHandle,  100);
  /* USER CODE END RTOS_TIMERS */

  /* Create the thread(s) */
  /* definition and creation of Motor1Control */
  osThreadDef(Motor1Control, vTaskMotor1Control, osPriorityNormal, 0, 128);
  Motor1ControlHandle = osThreadCreate(osThread(Motor1Control), NULL);

  /* definition and creation of Motor2Control */
  osThreadDef(Motor2Control, vTaskMotor2Control, osPriorityIdle, 0, 128);
  Motor2ControlHandle = osThreadCreate(osThread(Motor2Control), NULL);

  /* definition and creation of INPC1 */
  osThreadDef(INPC1, vTaskINPC1, osPriorityIdle, 0, 128);
  INPC1Handle = osThreadCreate(osThread(INPC1), NULL);

  /* definition and creation of INPC2 */
  osThreadDef(INPC2, vTaskINPC2, osPriorityIdle, 0, 128);
  INPC2Handle = osThreadCreate(osThread(INPC2), NULL);

  /* definition and creation of UartRX */
  osThreadDef(UartRX, vTaskUartRX, osPriorityIdle, 0, 128);
  UartRXHandle = osThreadCreate(osThread(UartRX), NULL);

  /* definition and creation of UartTX */
  osThreadDef(UartTX, vTaskUartTX, osPriorityIdle, 0, 128);
  UartTXHandle = osThreadCreate(osThread(UartTX), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* Create the queue(s) */
  /* definition and creation of motorSpeed */
  osMessageQDef(motorSpeed, 128, uint16_t);
  motorSpeedHandle = osMessageCreate(osMessageQ(motorSpeed), NULL);

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */
 

  /* Start scheduler */
  osKernelStart();
  
  /* We should never get here as control is now taken by the scheduler */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI_DIV2;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL16;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_SlaveConfigTypeDef sSlaveConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_IC_InitTypeDef sConfigIC = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 64-1;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 10000;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_IC_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sSlaveConfig.SlaveMode = TIM_SLAVEMODE_RESET;
  sSlaveConfig.InputTrigger = TIM_TS_TI1FP1;
  sSlaveConfig.TriggerPolarity = TIM_INPUTCHANNELPOLARITY_RISING;
  sSlaveConfig.TriggerFilter = 0;
  if (HAL_TIM_SlaveConfigSynchronization(&htim1, &sSlaveConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigIC.ICPolarity = TIM_INPUTCHANNELPOLARITY_RISING;
  sConfigIC.ICSelection = TIM_ICSELECTION_DIRECTTI;
  sConfigIC.ICPrescaler = TIM_ICPSC_DIV1;
  sConfigIC.ICFilter = 0;
  if (HAL_TIM_IC_ConfigChannel(&htim1, &sConfigIC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigIC.ICPolarity = TIM_INPUTCHANNELPOLARITY_FALLING;
  sConfigIC.ICSelection = TIM_ICSELECTION_INDIRECTTI;
  if (HAL_TIM_IC_ConfigChannel(&htim1, &sConfigIC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 3;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 1000;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_PWM_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */
  HAL_TIM_MspPostInit(&htim2);

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 3;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 1000;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_PWM_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */
  HAL_TIM_MspPostInit(&htim3);

}

/**
  * @brief TIM4 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM4_Init(void)
{

  /* USER CODE BEGIN TIM4_Init 0 */

  /* USER CODE END TIM4_Init 0 */

  TIM_SlaveConfigTypeDef sSlaveConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_IC_InitTypeDef sConfigIC = {0};

  /* USER CODE BEGIN TIM4_Init 1 */

  /* USER CODE END TIM4_Init 1 */
  htim4.Instance = TIM4;
  htim4.Init.Prescaler = 64-1;
  htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim4.Init.Period = 10000;
  htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim4.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim4) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_IC_Init(&htim4) != HAL_OK)
  {
    Error_Handler();
  }
  sSlaveConfig.SlaveMode = TIM_SLAVEMODE_RESET;
  sSlaveConfig.InputTrigger = TIM_TS_TI1FP1;
  sSlaveConfig.TriggerPolarity = TIM_INPUTCHANNELPOLARITY_RISING;
  sSlaveConfig.TriggerFilter = 0;
  if (HAL_TIM_SlaveConfigSynchronization(&htim4, &sSlaveConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigIC.ICPolarity = TIM_INPUTCHANNELPOLARITY_RISING;
  sConfigIC.ICSelection = TIM_ICSELECTION_DIRECTTI;
  sConfigIC.ICPrescaler = TIM_ICPSC_DIV1;
  sConfigIC.ICFilter = 0;
  if (HAL_TIM_IC_ConfigChannel(&htim4, &sConfigIC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigIC.ICPolarity = TIM_INPUTCHANNELPOLARITY_FALLING;
  sConfigIC.ICSelection = TIM_ICSELECTION_INDIRECTTI;
  if (HAL_TIM_IC_ConfigChannel(&htim4, &sConfigIC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM4_Init 2 */

  /* USER CODE END TIM4_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13, GPIO_PIN_RESET);

  /*Configure GPIO pin : PC13 */
  GPIO_InitStruct.Pin = GPIO_PIN_13;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : PB13 */
  GPIO_InitStruct.Pin = GPIO_PIN_13;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

uint32_t SpeedSetValue[NUMBER_OF_MOTORS] = {0,0};
uint32_t RampDelayCnt[NUMBER_OF_MOTORS] = {0,0};
uint32_t RampDelayMax[NUMBER_OF_MOTORS] = {0,0};

void SpeedRampGen(enmMotorDriver driver )
{
    int32_t Temp; 
    
    Temp = PIDParams_t[driver].SpeedRef - SpeedSetValue[driver];
    
    if((Temp >= 100) || (Temp <= (-100)))
    {
        RampDelayCnt[driver]++;
        if(RampDelayCnt[driver] >= RampDelayMax[driver])
        {
            if(PIDParams_t[driver].SpeedRef >= SpeedSetValue[driver])
                SpeedSetValue[driver] += 100;
            else
                SpeedSetValue[driver] -= 100;
            
            sLIMIT_VARIABLE(SpeedSetValue[driver], u16MotorSpeedMaxValue, u16MotorSpeedMinValue);
            RampDelayCnt[driver] = 0;
        }
    }
}

void ResetPID(enmMotorDriver driver )
{
	if((driver < eMotorMax))
	{
        PIDParams_t[driver].DutyCycle = 0;
        PIDParams_t[driver].SpeedRef = 0;
        PIDParams_t[driver].PreviousError = 0;
        PIDParams_t[driver].ErrorSum = 0;
        PIDParams_t[driver].Kp = AppPIKp;
        PIDParams_t[driver].Ki = AppPIKi;
        PIDParams_t[driver].Kd = AppPIKd;
	}
}

void Motor1PWMSetValue(uint16_t value, uint8_t direction) 
{
    TIM_OC_InitTypeDef sConfigOC;
    
    sConfigOC.OCMode = TIM_OCMODE_PWM1;
    sConfigOC.Pulse = value;
    sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
    sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;

    HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_3);
    HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_4);

    if(direction < eMotorMovMax)
    {
        if(direction == 0)
        {
            HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_3);
            //HAL_TIM_PWM_Stop(&htim2, TIM_CHANNEL_4);
        }
        else if(direction == 1)
        {
            HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_4);
            //HAL_TIM_PWM_Stop(&htim2, TIM_CHANNEL_3);
        }
        else
        {
            /* code */
            sConfigOC.Pulse = 0;
            HAL_TIM_PWM_Stop(&htim2, TIM_CHANNEL_3);
            HAL_TIM_PWM_Stop(&htim2, TIM_CHANNEL_4);
        }
        
    }
}

void Motor2PWMSetValue(uint16_t value, uint8_t direction) 
{
    TIM_OC_InitTypeDef sConfigOC;
    
    sConfigOC.OCMode = TIM_OCMODE_PWM1;
    sConfigOC.Pulse = value;
    sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
    sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
    
    HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_1);
    HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_2);
    
    if(direction < eMotorMovMax)
    {
        if(direction == 0)
        {
            HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);
            //HAL_TIM_PWM_Stop(&htim3, TIM_CHANNEL_2);
        }
        else if(direction == 1)
        {
            HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2);
            //HAL_TIM_PWM_Stop(&htim3, TIM_CHANNEL_1);
        }
        else
        {
            /* code */
            sConfigOC.Pulse = 0;
            HAL_TIM_PWM_Stop(&htim3, TIM_CHANNEL_1);
            HAL_TIM_PWM_Stop(&htim3, TIM_CHANNEL_2);
        }
    }
}

void MotorSpeedRunPID(enmMotorDriver driver,uint32_t u32Speed)
{
	int32_t i32ControllerOutput = 0;
	int32_t i32Error=0;
	int32_t i32DiffError=0;

 	if(driver < eMotorMax)
	{
		if(PIDParams_t[driver].SpeedRef > 0)
		{
			i32Error = SpeedSetValue[driver] - u32Speed;
			
			PIDParams_t[driver].ErrorSum += i32Error;

			i32DiffError = i32Error - PIDParams_t[driver].PreviousError;
            
			PIDParams_t[driver].PreviousError = i32Error;
			
			sLIMIT_VARIABLE(PIDParams_t[driver].ErrorSum,i32PIDErrorSumLimit,-1*i32PIDErrorSumLimit);
			
			i32ControllerOutput = PIDParams_t[driver].Kp * i32Error + PIDParams_t[driver].Ki * (float)PIDParams_t[driver].ErrorSum;// + 
							//PIDParams_t[driver].Kd * (float)i32diff_error;
			
			sLIMIT_VARIABLE(i32ControllerOutput,i16PIDErrorLimit,-1*i16PIDErrorLimit);
                        
			PIDParams_t[driver].DutyCycle += i32ControllerOutput;

			sLIMIT_VARIABLE(PIDParams_t[driver].DutyCycle,i16PIDMaxOnTime,i16PIDMinOnTime);		
		}
		else
		{
			PIDParams_t[driver].DutyCycle = 0;
		}
	}
}

void CalculateSpeedMean(enmMotorDriver driver)
{   unsigned char cnt;
    uint32_t outMean=0;
    if(driver < eMotorMax)
    {   for (cnt=1; cnt < SAMPLE_DATA_COUNT;cnt++)
        {   outMean+=MotorSpeedValuesArray[driver][cnt];
        }
        CalcParams_t[driver].Speed=outMean / (SAMPLE_DATA_COUNT-1);
    }
}

static Error_t app_sensor_control_filter_values(enmMotorDriver driver)
{
    uint32_t outMedian;
    uint32_t outMean;
    uint32_t outDeviation;

    if(driver < eMotorMax)
    {
            app_get_statistics(&MotorSpeedValuesArray[driver][1], SAMPLE_DATA_COUNT-1, &outMedian, &outMean, &outDeviation);
//            if(outDeviation < APP_SENSOR_CONTROL_MAX_STD_DEVIATION)
//            {
//                if (!adc_filter_counter[driver])
//                {   SpeedValues[driver]=outMean;
//                    adc_filter_counter[driver]=1;
//                }
//                else{
                    SpeedValues[driver] = (SpeedValues[driver]*3  + outMedian) >> 2;
                    SpeedMeanValues[driver]=outMean*3;
//                }
                return E_SUCCESS;
//            }
//            else
//                return E_FAIL;
    }
    else
        return E_OUT_OF_RANGE;
}

float Saturate(float x, float llim, float hlim){
	if(x < llim)return llim;
	else if (x > hlim)return hlim;
	else return x;
}

/* USER CODE END 4 */

/* USER CODE BEGIN Header_vTaskMotor1Control */
/**
  * @brief  Function implementing the Motor1Control thread.
  * @param  argument: Not used 
  * @retval None
  */
/* USER CODE END Header_vTaskMotor1Control */
void vTaskMotor1Control(void const * argument)
{

  /* USER CODE BEGIN 5 */
  /* Infinite loop */
  for(;;)
  {    
        //UserPWMSetValue(PIDParams_t[eMotor1].DutyCycle);
        //UserPWMSetValue(u32OpenDutyCycle);
        //UserPWMSetValue((uint32_t)fDutyRef[eMotor1]);
#ifdef OPEN_TEST
        if(u8MotorDirection[eMotor1] == eMotorForward)
        {
            Motor1PWMSetValue(u32OpenDutyCycle, eMotorForward);
        }
        else if(u8MotorDirection[eMotor1] == eMotorBackward)
        {
            Motor1PWMSetValue(u32OpenDutyCycle, eMotorBackward);
        }
        else
        {
            Motor1PWMSetValue(0, eMotorMax);
        }
#else
        if(u8MotorDirection[eMotor1] == eMotorForward)
        {
            Motor1PWMSetValue(PIDParams_t[eMotor1].DutyCycle, eMotorForward);
        }
        else if(u8MotorDirection[eMotor1] == eMotorBackward)
        {
            Motor1PWMSetValue(PIDParams_t[eMotor1].DutyCycle, eMotorBackward);
        }
        else
        {
            Motor1PWMSetValue(0, eMotorMax);
        }
#endif
        
      osDelay(1);
  }
  /* USER CODE END 5 */ 
}

/* USER CODE BEGIN Header_vTaskMotor2Control */
/**
* @brief Function implementing the Motor2Control thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_vTaskMotor2Control */
void vTaskMotor2Control(void const * argument)
{
  /* USER CODE BEGIN vTaskMotor2Control */
  /* Infinite loop */
  for(;;)
  {
        //UserPWMSetValue(PIDParams_t[eMotor2].DutyCycle);
        //UserPWMSetValue(u32OpenDutyCycle);
        //UserPWMSetValue((uint32_t)fDutyRef[eMotor2]);
#ifdef OPEN_TEST
        if(u8MotorDirection[eMotor2] == eMotorForward)
        {
            Motor2PWMSetValue(u32OpenDutyCycle, eMotorForward);
        }
        else if(u8MotorDirection[eMotor2] == eMotorBackward)
        {
            Motor2PWMSetValue(u32OpenDutyCycle, eMotorBackward);
        }
        else
        {
            Motor2PWMSetValue(0, eMotorMax);
        }
#else
        if(u8MotorDirection[eMotor2] == eMotorForward)
        {
            Motor2PWMSetValue(PIDParams_t[eMotor2].DutyCycle, eMotorForward);
        }
        else if(u8MotorDirection[eMotor2] == eMotorBackward)
        {
            Motor2PWMSetValue(PIDParams_t[eMotor2].DutyCycle, eMotorBackward);
        }
        else
        {
            Motor2PWMSetValue(0, eMotorMax);
        }
#endif
        
      osDelay(1);
  }
  /* USER CODE END vTaskMotor2Control */
}

/* USER CODE BEGIN Header_vTaskINPC1 */
/**
* @brief Function implementing the INPC1 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_vTaskINPC1 */
void vTaskINPC1(void const * argument)
{
  /* USER CODE BEGIN vTaskINPC1 */
    uint8_t cnt;
  /* Infinite loop */
  for(;;)
  {
      if( __HAL_TIM_GET_FLAG(&htim1, TIM_FLAG_CC1OF) &
          __HAL_TIM_GET_FLAG(&htim1, TIM_FLAG_CC2OF))
      {
          CalcParams_t[eMotor1].Ch1Rising = HAL_TIM_ReadCapturedValue(&htim1, TIM_CHANNEL_1);
          CalcParams_t[eMotor1].Ch1Falling = HAL_TIM_ReadCapturedValue(&htim1, TIM_CHANNEL_2);
          
          CalcParams_t[eMotor1].Freq = (1 / (float)CalcParams_t[eMotor1].Ch1Rising)*(64000000 / (TIM1->PSC+1));
          //CalcParams_t[eMotor1].DutyCycle = ((float)CalcParams_t[eMotor1].Ch1Falling /(float)CalcParams_t[eMotor1].Ch1Rising)*100;
          MotorSpeedValuesArray[eMotor1][cnt++] = CalcParams_t[eMotor1].Freq;
          
          
          if(cnt> (SAMPLE_DATA_COUNT -1))
              cnt = 0;
      }
    //osDelay(100);
  }
  /* USER CODE END vTaskINPC1 */
}

/* USER CODE BEGIN Header_vTaskINPC2 */
/**
* @brief Function implementing the INPC2 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_vTaskINPC2 */
void vTaskINPC2(void const * argument)
{
  /* USER CODE BEGIN vTaskINPC2 */
    uint8_t cnt;
  /* Infinite loop */
  for(;;)
  {
       if( __HAL_TIM_GET_FLAG(&htim4, TIM_FLAG_CC1OF) &
          __HAL_TIM_GET_FLAG(&htim4, TIM_FLAG_CC2OF))
      {
          CalcParams_t[eMotor2].Ch1Rising = HAL_TIM_ReadCapturedValue(&htim4, TIM_CHANNEL_1);
          CalcParams_t[eMotor2].Ch1Falling = HAL_TIM_ReadCapturedValue(&htim4, TIM_CHANNEL_2);
          
          CalcParams_t[eMotor2].Freq = (1 / (float)CalcParams_t[eMotor2].Ch1Rising)*(64000000 / (TIM4->PSC+1));
          //CalcParams_t[eMotor2].DutyCycle = ((float)CalcParams_t[eMotor2].Ch1Falling /(float)CalcParams_t[eMotor2].Ch1Rising)*100;
                    
        MotorSpeedValuesArray[eMotor2][cnt++] = CalcParams_t[eMotor2].Freq;
          
        if(cnt> (SAMPLE_DATA_COUNT -1))
            cnt = 0;
      }
  }
  /* USER CODE END vTaskINPC2 */
}

/* USER CODE BEGIN Header_vTaskUartRX */
/**
* @brief Function implementing the UartRX thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_vTaskUartRX */
void vTaskUartRX(void const * argument)
{
  /* USER CODE BEGIN vTaskUartRX */
    rx.motor1.u16allData = 0;
    rx.motor2.u16allData = 0;
  /* Infinite loop */
  for(;;)
  {
        if(xSemaphoreTake(xRxSemaphoreHandle, 100) == pdTRUE)
        {
            rx.startByte=data_array[0];
            rx.endByte=data_array[5];
            if(0xFF == rx.startByte && 0xFE == rx.endByte)
            {                
                //  rx.allData[0]= data_array[1]<<8 | data_array[0];
                //   rx.allData[1]= data_array[3]<<8 | data_array[2];
                //   rx.allData[2]= data_array[5]<<8 | data_array[4];
                memcpy(&rx,data_array,6);
                // rx.allData[0] =  rx.allData[1] =  rx.allData[2] = 0xFFFF;

                memcpy(rx.motor1.u08allData,data_array+1,2); 
                memcpy(rx.motor2.u08allData,data_array+3,2); 

                //sizeS= sizeof(rx.UartRXData );
                u8SemaphoreCnt++;
                //HAL_UART_Transmit(&huart2,(uint8_t *)&rx,6,100);
                //HAL_UART_Transmit(&huart2,(uint8_t *)&tx_data_array,6,100);
            }
        }
        
    osDelay(1);
  }
  /* USER CODE END vTaskUartRX */
}

/* USER CODE BEGIN Header_vTaskUartTX */
/**
* @brief Function implementing the UartTX thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_vTaskUartTX */
void vTaskUartTX(void const * argument)
{
  /* USER CODE BEGIN vTaskUartTX */
    memset(MotorEncPulseCnt, 0, 2);
    tx.startByte = 0xFF;
    tx.endByte = 0xFE;
  /* Infinite loop */
  for(;;)
  {
        tx.motor1.motorParams.motorDirection = 0;
        tx.motor1.motorParams.motorSpeed = MotorEncPulseCnt[eMotor1]; //SpeedMeanValues[eMotor1];
        tx.motor2.motorParams.motorDirection = 0;
        tx.motor2.motorParams.motorSpeed = MotorEncPulseCnt[eMotor2]; //SpeedMeanValues[eMotor2];
      
        tx_data_array[0] = tx.startByte;
        tx_data_array[5] = tx.endByte;
      memcpy(tx_data_array+1, tx.motor1.u08allData,2);
      memcpy(tx_data_array+3, tx.motor2.u08allData,2);
      
      //HAL_UART_Transmit(&huart2,(uint8_t *)&tx_data_array,6,100);
    osDelay(100);
  }
  /* USER CODE END vTaskUartTX */
}

/* vTimer1Callback1 function */
void vTimer1Callback1(void const * argument)
{
  /* USER CODE BEGIN vTimer1Callback1 */

    MotorSpeedRunPID(eMotor1, SpeedMeanValues[eMotor1]);
    MotorSpeedRunPID(eMotor2, SpeedMeanValues[eMotor2]);

  /* USER CODE END vTimer1Callback1 */
}

/* vTimer100msCallBack function */
void vTimer100msCallBack(void const * argument)
{
  /* USER CODE BEGIN vTimer100msCallBack */
    
    PIDParams_t[eMotor1].SpeedRef = rx.motor1.motorParams.motorSpeed;
    u8MotorDirection[eMotor1] = rx.motor1.motorParams.motorDirection;
    PIDParams_t[eMotor2].SpeedRef = rx.motor2.motorParams.motorSpeed;
    u8MotorDirection[eMotor2] = rx.motor2.motorParams.motorDirection;
    
    SpeedRampGen(eMotor1);
    SpeedRampGen(eMotor2);
    //CalculateSpeedMean(eMotor1);
    app_sensor_control_filter_values(eMotor1);
    app_sensor_control_filter_values(eMotor2);
    
    MotorEncPulseCnt[eMotor1] = SpeedMeanValues[eMotor1]/30;
    MotorEncPulseCnt[eMotor2] = SpeedMeanValues[eMotor2]/30;
    
    HAL_UART_Transmit(&huart2,(uint8_t *)&tx_data_array,6,100);
    
  /* USER CODE END vTimer100msCallBack */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
