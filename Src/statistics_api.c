 /*********************************************************************
* INCLUDES
*/
#include "main.h"

#define APP_STATISTICS_C
#include "statistics_api.h"

/*********************************************************************
* USER DEFINITIONS
*/


/*********************************************************************
* USER DEFINITIONS
*/

/*********************************************************************
* LOCAL CONSTANTS
*/

/*********************************************************************
* LOCAL TYPEDEFS
*/

/*********************************************************************
* LOCAL MACROS
*/

/*********************************************************************
* LOCAL VARIABLES
*/

/*********************************************************************
* LOCAL FUNCTION PROTOTYPES 
*/


static void swap(uint32_t *p,uint32_t *q) {
   uint32_t t;
   
   t=*p; 
   *p=*q; 
   *q=t;
}

/*Better performance than bubble sort*/
void shellsort(uint32_t num,uint32_t arr[])
{
    int i, j, k;
    for (i = num / 2; i > 0; i = i / 2)
    {
        for (j = i; j < num; j++)
        {
            for(k = j - i; k >= 0; k = k - i)
            {
                if (arr[k+i] >= arr[k])
                    break;
                else
                {
			swap(&arr[k],&arr[k+i]);
			/*
                    tmp = arr[k];
                    arr[k] = arr[k+i];
                    arr[k+i] = tmp;*/
                }
            }
        }
    }
}

static void bubble_sort(uint32_t n,uint32_t a[])
{
	uint32_t i,j;

	for(i=0;i<n-1;i++) {
		for(j=0;j<n-i-1;j++) {
			if(a[j]>a[j+1]){
				swap(&a[j],&a[j+1]);
			}
		}
	}	
}


static uint32_t getMedian(uint32_t n,uint32_t a[]) 
{ 
	if ((n & (~0x1)) == 0x01)// if odd number
	{
		return a[n>>1];
	}
	else
	{
		return (a[(n>>1) - 1] + a[n>>1]) >> 1; // (a[(n / 2) - 1] + a[n / 2]) / 2.0; 
	}
}

static uint32_t getMean(uint32_t n,uint32_t a[]) { 
	uint32_t i;
	uint32_t sum = 0;
	for(i=1;i<n-1;i++) {
		sum += a[i];
	}
	return sum/(n-2);
}

static uint32_t getVariance(uint32_t n,uint32_t a[], uint32_t mean)
{
	uint32_t temp = 0;
        uint32_t i;
	for(i=1; i< n-1; i++){
		temp += ((a[i]-mean)*(a[i]-mean));
	}
	return temp/(n-3);
}

/*********************************************************************
* INTERFACE FUNCTIONS
*/

void app_get_statistics(uint32_t *data, uint32_t size, uint32_t *outMedian, uint32_t *outMean, uint32_t *outDeviation) 
{
	uint32_t variance;
	//bubble_sort(size,data); //sprt data
	shellsort(size,data);
	*outMedian = getMedian(size,data);
	*outMean = getMean(size,data);
	variance = getVariance(size,data,(*outMean));
	//*outDeviation = getStdDeviation(variance);
}    
